# find-the-princess

This program brute forces solutions for the princess door riddle
explained in this [YouTube video][princess-door-riddle]. It can find
solutions for a castle with any number of doors.

[princess-door-riddle]: https://youtu.be/nv0Onj3wXCE

As there is an easy solution to this riddle that can be calculated in
linear time for any number of doors, this brute force solution is
unlikely to be of any use to anyone, but can still be used to find
alternative solutions to those the general solution gives.

## Running

This project is currently in a state, in which it can not be
conveniently run yet.
