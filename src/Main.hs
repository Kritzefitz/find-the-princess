-- Copyright © 2017 Sven Bartscher
--
-- This file is part of find-the-princess.
--
-- find-the-princess is free software: you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- find-the-princess is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with find-the-princess.  If not, see
-- <http://www.gnu.org/licenses/>.

{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import Data.Maybe (isJust)
import qualified Data.Sequence as S
--import qualified Data.Text as T

newtype Castle = Castle Word
    deriving (Show, Eq)

newtype Princess = Princess Word
    deriving (Show, Eq)

newtype Prince = Prince Word
    deriving (Show, Eq)

data Positions = Positions !Prince !Princess
                 deriving (Show, Eq)

newtype Strategy = Strategy [Word]
    deriving (Show)

data GameState = GameState !Castle [(Maybe Positions, [Positions])]
               deriving (Show)

enhanceStrategy :: Word -> Strategy -> Strategy
enhanceStrategy x (Strategy xs) = Strategy $ x : xs

noStrategy :: Strategy
noStrategy = Strategy []

princessPos :: Positions -> Princess
princessPos (Positions _ p) = p

isWon :: GameState -> Bool
isWon (GameState _ histories) = all (isJust . fst) histories

doorChar :: Char
doorChar = '\x1f6aa'

princessChar :: Char
princessChar = '\x1f478'

princeChar :: Char
princeChar = '\x1f934'

princeTurn :: Castle -> [(Word, Prince)]
princeTurn (Castle n) = map (\p -> (p, Prince p)) [1..n]

initialPrincessTurn :: Castle -> Prince -> [Princess]
initialPrincessTurn (Castle n) (Prince pn) =
    map Princess $ filter (/=pn) [1..n]

princessTurn :: Castle -> Prince -> Princess -> [Princess]
princessTurn (Castle n) (Prince p) (Princess ps) =
    map Princess $ filter (\ps' -> ps' > 0 && ps' <= n && ps' /= p) [ps-1, ps+1]

startPositions :: Castle -> [(Strategy, GameState)]
startPositions c = map (\(pTurn, prince) -> ( enhanceStrategy pTurn noStrategy
                                            , GameState c $
                                              map (((,) Nothing) . pure . Positions prince) $
                                                  initialPrincessTurn c prince
                                            )
                       ) $ princeTurn c

advanceStrategy :: (Strategy, GameState) -> [(Strategy, GameState)]
advanceStrategy (currentStrategy, GameState castle histories) =
    map (\(pTurn, prince) -> ( enhanceStrategy pTurn currentStrategy
                             , GameState castle $ playPrincess castle prince histories
                             )
        ) $ princeTurn castle

playPrincess :: Castle
             -> Prince
             -> [(Maybe Positions, [Positions])]
             -> [(Maybe Positions, [Positions])]
playPrincess castle prince histories = do
  history <- histories
  case history of
    r@(Just _, _) -> return r
    (Nothing, positions@((Positions _ princess):_)) ->
        case princessTurn castle prince princess of
          [] -> return (Just $ Positions prince princess, positions)
          newPositions -> map (((,) Nothing) . (:positions) . Positions prince) newPositions

allSolutions :: Castle -> [(Strategy, GameState)]
allSolutions = filter (isWon . snd) . allStrategies

allStrategies :: Castle -> [(Strategy, GameState)]
allStrategies castle = let initialFIFO = S.fromList $ startPositions castle
                       in processFIFO initialFIFO
    where processFIFO fifo = case S.viewl fifo of
                               S.EmptyL -> []
                               (stratEntry@(_, game) S.:< restFIFO) ->
                                   if isWon game
                                   then stratEntry : processFIFO restFIFO
                                   else let nextStrategies = advanceStrategy stratEntry
                                        in processFIFO $ restFIFO S.>< S.fromList nextStrategies

main :: IO ()
main = putStrLn "Not implemented"
